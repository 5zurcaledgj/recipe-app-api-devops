#!/bin/sh

set -e

# this is a python command to collect all of the static files of the project
# so you can set up the nginx proxy to serve them
python manage.py collectstatic --noinput

# python will wait for the db and make sure that the database
# is up and running
python manage.py wait_for_db

# you have to make sure that all of the migrations need for the database
# is also for the production
python manage.py migrate

# run the uWSGI service itself
# Run uWSGI as a TCP socket on port 9000. This way we can our request from our proxy to this port using uwsgi path that we set up in our application
# --master flag means run this as a master service on the terminals
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi